
# Notes

# Pulling Images

## Pulling for Development
The following will pull the most recently built image into an "images" subdirectory of your /work directory, (/work/YOUR_NETID/images). 

```
IMAGEDIR=/work/${USER}/images
mkdir -p $IMAGEDIR
singularity pull --force --dir $IMAGEDIR oras://gitlab-registry.oit.duke.edu/granek-lab/granek-container-images/amplicon-sim-image:latest
```

## Pulling for Production
The following command will pull the image with the tag **TAG_NAME** You must replace **TAG_NAME** with the commit tag of the image you want to pull:

```
IMAGEDIR=/work/${USER}/images
mkdir -p $IMAGEDIR
singularity pull --force --dir $IMAGEDIR oras://gitlab-registry.oit.duke.edu/granek-lab/granek-container-images/amplicon-sim-image:TAG_NAME
```